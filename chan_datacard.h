/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

#ifndef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef STR_AND_LEN
#define STR_AND_LEN(str) str, (sizeof(str) - 1)
#else
#error Something wrong STR_AND_LEN already defined
#endif

#ifndef LEN
#define LEN(str) (sizeof(str) - 1)
#else
#error Something wrong LEN already defined
#endif

#define DISCOVERY_INT	60
#define FRAME_SIZE	320

struct datacard_pvt
{
	AST_LIST_ENTRY(datacard_pvt) entry;

	ast_mutex_t		lock;				/* pvt lock */
	pthread_t		thread;				/* thread handle */

	int			audio_fd;			/* audio descriptor */
	int			data_fd;			/* data descriptor */
	int			event_fd;			/* event descriptor */

	struct ast_channel	*owner;				/* Channel we belong to, possibly NULL */
	struct ast_dsp		*dsp;
	struct ast_timer	*audio_timer;

	char			audio_write_buf[FRAME_SIZE * 5];
	struct ringbuffer	audio_write_rb;
	char			audio_read_buf[FRAME_SIZE + AST_FRIENDLY_OFFSET];
	struct ast_frame	audio_read_frame;

	struct at_entry		data_at_entry[10];
	struct at_buffer	data_at_buf;

	int			timeout;

	int			rssi;
	int			linkmode;
	int			linksubmode;
	int			gsm_status;

	/* flags */
	unsigned int		connected:1;			/* do we have an connection to a datacard */
	unsigned int		initialized:1;			/* whether a service level connection exists or not */
	unsigned int		gsm_registered:1;		/* do we have an registration to a GSM */

	unsigned int		offline:1;

	unsigned int		incoming:1;			/* incoming call */
	unsigned int		outgoing:1;			/* outgoing call */

	unsigned int		ringing:1;
	unsigned int		talking:1;

	unsigned int		incoming_sms:1; // ???		/* incoming sms */
	unsigned int		outgoing_sms:1; // ???		/* outgoing sms */

	unsigned int		needchup:1; // ???		/* we need to send a CHUP */

	unsigned int		group_last_used:1;		/* mark the last used datacard */
	unsigned int		sim_last_used:1;		/* mark the last used datacard */

	unsigned int		has_sms:1;
	unsigned int		has_voice:1;
	unsigned int		use_ucs2_encoding:1;
	unsigned int		ussd_use_7bit_encoding:1;
	unsigned int		ussd_use_ucs2_encoding:1;
	unsigned int		ussd_use_ucs2_decoding:1;

	/* Config */
	int			group;				/* group number for group dialling */
	int			rxgain;				/* increase the incoming volume */
	int			txgain;				/* increase the outgoint volume */
	int			callingpres;			/* calling presentation */
	unsigned int		usecallingpres:1;

	struct ast_variable	*vars;				/* Variables to set for channel created by user */

	AST_DECLARE_STRING_FIELDS(
		AST_STRING_FIELD(id);				/* id from datacard.conf */
		AST_STRING_FIELD(audio_tty);			/* tty for audio connection */
		AST_STRING_FIELD(data_tty);			/* tty for AT commands */
		AST_STRING_FIELD(event_tty);			/* tty for events */
		AST_STRING_FIELD(context);			/* the context for incoming calls */
		AST_STRING_FIELD(language);			/* Preferred language */
		AST_STRING_FIELD(provider);
		AST_STRING_FIELD(manufacturer);
		AST_STRING_FIELD(model);
		AST_STRING_FIELD(firmware);
		AST_STRING_FIELD(imei);
		AST_STRING_FIELD(imsi);
		AST_STRING_FIELD(number);
		AST_STRING_FIELD(location_area_code);
		AST_STRING_FIELD(cell_id);
	);
};

/*! Global jitterbuffer configuration - by default, jb is disabled */
static struct ast_jb_conf jbconf_default = {
	.flags			= 0,
	.max_size		= -1,
	.resync_threshold	= -1,
	.impl			= "",
	.target_extra		= -1,
};

static struct ast_jb_conf jbconf_global;

static char silence_frame[FRAME_SIZE];

static char default_language[MAX_LANGUAGE] = "en";		/* Default language setting for new channels */
static int relaxdtmf_global;					/* Relax DTMF */

static int discovery_interval = DISCOVERY_INT;			/* The datacard discovery interval */


AST_MUTEX_DEFINE_STATIC(unload_mtx);
static int unload_flag = 0;
static int check_unloading();

static AST_RWLIST_HEAD_STATIC(datacards, datacard_pvt);

AST_MUTEX_DEFINE_STATIC(round_robin_mtx);
static struct datacard_pvt *round_robin[64];
