/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static int	manager_action_show_devices	(struct mansession *, const struct message *);
static int	manager_action_show_status	(struct mansession *, const struct message *);
//static int	manager_action_send_sms		(struct mansession *, const struct message *);
static int	manager_action_send_ussd	(struct mansession *, const struct message *);
static int	manager_action_reboot		(struct mansession *, const struct message *);
static void	manager_event_new_sms_base64	(struct datacard_pvt *, char *, char *);
static void	manager_event_new_ussd_base64	(struct datacard_pvt *, char *);
static void	manager_event_status		(struct datacard_pvt *);
