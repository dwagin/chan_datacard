/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static int at_write(const char *datacard, int fd, const char *buf, size_t len)
{
	ssize_t	out;

	ast_debug(5, "[%s] [%.*s]\n", datacard, (int) len, buf);

	while (len > 0)
	{
		if ((out = write(fd, buf, len)) < 0)
		{
			ast_log(LOG_ERROR, "[%s] write() error: %s\n", datacard, strerror(errno));
			return -1;
		}

		len -= out;
		buf += out;
	}

	return 0;
}

static int at_send_entry(struct datacard_pvt *pvt)
{
	struct at_entry *e;

	e = at_buf_read(&pvt->data_at_buf);

	if (e)
	{
		e->timeout = ast_tvadd(ast_tvnow(), ast_tv(5, 0));
		return at_write(pvt->id, pvt->data_fd, e->buf, e->len);
	}

	return 0;
}

static int at_send_buf(struct datacard_pvt *pvt, at_command_t cmd, at_response_t res, const char *buf, size_t len)
{
	struct at_entry *e;

	e = at_buf_write(&pvt->data_at_buf);

	if (e && len <= sizeof(e->buf) && len > 0)
	{
		e->command  = cmd;
		e->response = res;
		e->len      = len;

		memcpy(e->buf, buf, len);

		at_buf_write_finish(&pvt->data_at_buf);

		if (at_buf_used(&pvt->data_at_buf) == 1)
		{
			e->timeout = ast_tvadd(ast_tvnow(), ast_tv(5, 0));
			return at_write(pvt->id, pvt->data_fd, e->buf, e->len);
		}
		else
		{
			e->timeout = (struct timeval) { 0, 0 };
		}

		return 0;
	}
	else
	{
		ast_log(LOG_ERROR, "[%s] AT buffer overflow\n\n", pvt->id);
	}

	return -1;
}

static int at_send_at(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT, RES_OK, STR_AND_LEN("AT\r"));
}

static int at_send_atz(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_Z, RES_OK, STR_AND_LEN("ATZ\r"));
}

static int at_send_ate0(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_E, RES_OK, STR_AND_LEN("ATE0\r"));
}

static int at_send_cmee(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CMEE, RES_OK, STR_AND_LEN("AT+CMEE=1\r"));
}

static int at_send_cgmi(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CGMI, RES_OK, STR_AND_LEN("AT+CGMI\r"));
}

static int at_send_cgmm(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CGMM, RES_OK, STR_AND_LEN("AT+CGMM\r"));
}

static int at_send_cgmr(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CGMR, RES_OK, STR_AND_LEN("AT+CGMR\r"));
}

static int at_send_cgsn(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CGSN, RES_OK, STR_AND_LEN("AT+CGSN\r"));
}

static int at_send_u2diag0(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_U2DIAG, RES_OK, STR_AND_LEN("AT^U2DIAG=0\r"));
}

static int at_send_cimi(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CIMI, RES_OK, STR_AND_LEN("AT+CIMI\r"));
}

static int at_send_cpin_test(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CPIN_TEST, RES_OK, STR_AND_LEN("AT+CPIN?\r"));
}

static int at_send_cnum(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CNUM, RES_OK, STR_AND_LEN("AT+CNUM\r"));
}

static int at_send_clip(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CLIP, RES_OK, STR_AND_LEN("AT+CLIP=1\r"));
}

static int at_send_cops_init(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_COPS_INIT, RES_OK, STR_AND_LEN("AT+COPS=0,0\r"));
}

static int at_send_cops(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_COPS, RES_OK, STR_AND_LEN("AT+COPS?\r"));
}

static int at_send_creg_init(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CREG_INIT, RES_OK, STR_AND_LEN("AT+CREG=2\r"));
}

static int at_send_creg(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CREG, RES_OK, STR_AND_LEN("AT+CREG?\r"));
}

static int at_send_stsf(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_STSF, RES_OK, STR_AND_LEN("AT^STSF=0\r"));
}

static int at_send_cvoice_test(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CVOICE, RES_OK, STR_AND_LEN("AT^CVOICE?\r"));
}

static int at_send_cssn(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CSSN, RES_OK, STR_AND_LEN("AT+CSSN=1,1\r"));
}

static int at_send_cmgf(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CMGF, RES_OK, STR_AND_LEN("AT+CMGF=1\r"));
}

static int at_send_cscs_ucs2(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CSCS, RES_OK, STR_AND_LEN("AT+CSCS=\"UCS2\"\r"));
}

static int at_send_cpms(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CPMS, RES_OK, STR_AND_LEN("AT+CPMS=\"MT\",\"ME\",\"ME\"\r"));
}

static int at_send_cnmi(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CNMI, RES_OK, STR_AND_LEN("AT+CNMI=2,1,0,0,0\r"));
}

static int at_send_curc(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CURC, RES_OK, STR_AND_LEN("AT^CURC=1\r"));
}

static int at_send_csq(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CSQ, RES_OK, STR_AND_LEN("AT+CSQ\r"));
}

static int at_send_ata(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_A, RES_OK, STR_AND_LEN("ATA\r"));
}

static int at_send_clir(struct datacard_pvt *pvt, int mode)
{
	char buf[32];
	size_t len;

	len = snprintf(buf, sizeof(buf), "AT+CLIR=%d\r", mode);

	return at_send_buf(pvt, CMD_AT_CLIR, RES_OK, buf, MIN(len, sizeof(buf)));
}

static int at_send_atd(struct datacard_pvt *pvt, const char *number)
{
	char buf[64];
	size_t len;

	len = snprintf(buf, sizeof(buf), "ATD%s;\r", number);

	return at_send_buf(pvt, CMD_AT_D, RES_OK, buf, MIN(len, sizeof(buf)));
}

static int at_send_ddsetex(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_DDSETEX, RES_OK, STR_AND_LEN("AT^DDSETEX=2\r"));
}

static int at_send_clvl(struct datacard_pvt *pvt, int level)
{
	char buf[32];
	size_t len;

	len = snprintf(buf, sizeof(buf), "AT+CLVL=%d\r", level);

	return at_send_buf(pvt, CMD_AT_CLVL, RES_OK, buf, MIN(len, sizeof(buf)));
}

static int at_send_chup(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CHUP, RES_OK, STR_AND_LEN("AT+CHUP\r"));
}

static int at_send_dtmf(struct datacard_pvt *pvt, char digit)
{
	char buf[16];
	size_t len;

	switch (digit)
	{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case '*':
		case '#':
			len = snprintf(buf, sizeof(buf), "AT^DTMF=1,%c\r", digit);
			return at_send_buf(pvt, CMD_AT_DTMF, RES_OK, buf, MIN(len, sizeof(buf)));
	}

	return -1;
}

static int at_send_cusd(struct datacard_pvt *pvt, const char *code)
{
	char buf[256];
	char *p = buf;
	ssize_t res;

	static const char str1[] = "AT+CUSD=1,\"";
	static const char str2[] = "\",15\r";

	memcpy(buf, STR_AND_LEN(str1));

	p += LEN(str1);

	if (pvt->ussd_use_7bit_encoding)
	{
		if ((res = conv_utf8_to_latin1_7bit_hexstr(code, strlen(code), p, sizeof(buf) - (LEN(str1) + LEN(str2)))) <= 0)
		{
			ast_log(LOG_ERROR, "[%s] Error converting USSD code to 7bit: %s\n", pvt->id, code);
			return -1;
		}
	}
	else if (pvt->ussd_use_ucs2_encoding)
	{
		if ((res = conv_utf8_to_ucs2_8bit_hexstr(code, strlen(code), p, sizeof(buf) - (LEN(str1) + LEN(str2)))) <= 0)
		{
			ast_log(LOG_ERROR, "[%s] Error converting USSD code to UCS-2: %s\n", pvt->id, code);
			return -1;
		}
	}
	else
	{
		res = MIN(strlen(code), sizeof(buf) - (LEN(str1) + LEN(str2)));
		memcpy(p, code, res);
	}

	p += res;

	memcpy(p, STR_AND_LEN(str2));

	p += LEN(str2);

	return at_send_buf(pvt, CMD_AT_CUSD, RES_OK, buf, p - buf);
}

static int at_send_cmgr(struct datacard_pvt *pvt, int index)
{
	char buf[32];
	size_t len;

	len = snprintf(buf, sizeof(buf), "AT+CMGR=%d\r", index);

	return at_send_buf(pvt, CMD_AT_CMGR, RES_OK, buf, MIN(len, sizeof(buf)));
}

static int at_send_cmgd(struct datacard_pvt *pvt, int index, int flag)
{
	char buf[32];
	size_t len;

	len = snprintf(buf, sizeof(buf), "AT+CMGD=%d,%d\r", index, flag);

	return at_send_buf(pvt, CMD_AT_CMGD, RES_OK, buf, MIN(len, sizeof(buf)));
}

static int at_send_ccwa_disable(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CCWA, RES_OK, STR_AND_LEN("AT+CCWA=0,0,1\r"));
}

static int at_send_cfun_reboot(struct datacard_pvt *pvt)
{
	return at_send_buf(pvt, CMD_AT_CFUN, RES_OK, STR_AND_LEN("AT+CFUN=1,1\r"));
}

static int at_send_cpin(struct datacard_pvt *pvt, const char *pin)
{
	char buf[32];
	size_t len;

	len = snprintf(buf, sizeof(buf), "AT+CPIN=\"%s\"\r", pin);

	return at_send_buf(pvt, CMD_AT_CPIN, RES_OK, buf, MIN(len, sizeof(buf)));
}

static int at_send_clck(struct datacard_pvt *pvt, const char *pin)
{
	char buf[32];
	size_t len;

	len = snprintf(buf, sizeof(buf), "AT+CLCK=\"SC\",0,\"%s\"\r", pin);

	return at_send_buf(pvt, CMD_AT_CLCK, RES_OK, buf, MIN(len, sizeof(buf)));
}
