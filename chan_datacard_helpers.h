/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

#ifndef __HELPERS_H__
#define __HELPERS_H__

enum dcs_alphabet
{
	ALPHABET_UNKNOWN,
	ALPHABET_GSM,
	ALPHABET_8BIT,
	ALPHABET_UCS2,
	ALPHABET_RESERVED,
};

static int			tty_open			(const char *);
static void			tty_close			(int *);
static int			tty_status			(int);
static int			can_incoming			(struct datacard_pvt *);
static int			can_outgoing			(struct datacard_pvt *);
static int			can_ussd			(struct datacard_pvt *);
static int			can_sms				(struct datacard_pvt *);
static const char		*datacard_gsm_status_txt	(struct datacard_pvt *);
static const char		*datacard_status_txt		(struct datacard_pvt *);
static struct ast_variable	*parse_setvar			(const char *, struct ast_variable *);
static struct datacard_pvt	*find_datacard			(const char *);
static char			*complete_datacard		(const char *, const char *, int, int, int);
static int			get_at_clir_value		(const char *, int);
static char			*rssi2dBm			(int, char *, size_t);
static void			trimnr				(char *, size_t *, const char *);
static int			parse_dcs			(int);

#endif /* __HELPERS_H__ */
