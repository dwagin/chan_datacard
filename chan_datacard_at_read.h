/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static int	at_wait_read	(const char *, int *, int, int);
static ssize_t	at_read		(const char *, const char *, int, char *, size_t);
