/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static int manager_action_show_devices(struct mansession *s, const struct message *m)
{
	const char *id = astman_get_header(m, "ActionID");

	char idtext[256] = "";
	struct datacard_pvt *pvt;
	size_t count = 0;

	if (!ast_strlen_zero(id))
	{
		snprintf(idtext, sizeof(idtext), "ActionID: %s\r\n", id);
	}

	astman_send_listack(s, m, "Datacard list will follow", "start");

	AST_RWLIST_RDLOCK(&datacards);
	AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
	{
		pvt_lock(pvt);
		astman_append(s,
			"Event: DatacardDeviceEntry\r\n"
			"%s"
			"Device: %s\r\n"
			"Context: %s\r\n"
			"Language: %s\r\n"
			"Group: %d\r\n"
			"GSM status: %s\r\n"
			"Status: %s\r\n", 
			idtext,
			pvt->id,
			pvt->context,
			pvt->language,
			pvt->group,
			datacard_gsm_status_txt(pvt),
			datacard_status_txt(pvt)
		);
		astman_append(s,
			"Voice: %s\r\n"
			"SMS: %s\r\n"
			"RSSI: %d\r\n"
			"Mode: %d\r\n"
			"Submode: %d\r\n"
			"Provider: %s\r\n"
			"Manufacturer: %s\r\n"
			"Model: %s\r\n"
			"Firmware: %s\r\n"
			"IMEI: %s\r\n"
			"IMSI: %s\r\n"
			"Number: %s\r\n"
			"Use CallingPres: %s\r\n"
			"Default CallingPres: %s\r\n"
			"Use UCS-2 encoding: %s\r\n"
			"USSD use 7 bit encoding: %s\r\n"
			"USSD use UCS-2 encoding: %s\r\n"
			"USSD use UCS-2 decoding: %s\r\n"
			"Location area code: %s\r\n"
			"Cell ID: %s\r\n"
			"\r\n",
			(pvt->has_voice) ? "Yes" : "No",
			(pvt->has_sms) ? "Yes" : "No",
			pvt->rssi,
			pvt->linkmode,
			pvt->linksubmode,
			pvt->provider,
			pvt->manufacturer,
			pvt->model,
			pvt->firmware,
			pvt->imei,
			pvt->imsi,
			pvt->number,
			pvt->usecallingpres ? "Yes" : "No",
			pvt->callingpres < 0 ? "<Not set>" : ast_describe_caller_presentation(pvt->callingpres),
			pvt->use_ucs2_encoding ? "Yes" : "No",
			pvt->ussd_use_7bit_encoding ? "Yes" : "No",
			pvt->ussd_use_ucs2_encoding ? "Yes" : "No",
			pvt->ussd_use_ucs2_decoding ? "Yes" : "No",
			pvt->location_area_code,
			pvt->cell_id
		);
		pvt_unlock(pvt);
		count++;
	}
	AST_RWLIST_UNLOCK(&datacards);

	astman_append(s,
		"Event: DatacardShowDevicesComplete\r\n%s"
		"EventList: Complete\r\n"
		"ListItems: %zu\r\n"
		"\r\n",
		idtext, count
	);

	return 0;
}

static int manager_action_show_status(struct mansession *s, const struct message *m)
{
	const char *id = astman_get_header(m, "ActionID");
	const char *datacard = astman_get_header(m, "Device");

	char idtext[256] = "";
	struct datacard_pvt *pvt;
	char buf[256];
	size_t count = 0;

	if (!ast_strlen_zero(id))
	{
		snprintf(idtext, sizeof(idtext), "ActionID: %s\r\n", id);
	}

	if (!ast_strlen_zero(datacard))
	{
		pvt = find_datacard(datacard);

		if (pvt)
		{
			pvt_lock(pvt);
			astman_append(s,
				"Event: DatacardStatusEntry\r\n"
				"%s"
				"Device: %s\r\n"
				"IMEI: %s\r\n"
				"IMSI: %s\r\n"
				"Number: %s\r\n"
				"Status: %s\r\n"
				"\r\n",
				idtext,
				pvt->id,
				pvt->imei,
				pvt->imsi,
				pvt->number,
				datacard_status_txt(pvt)
			);
			pvt_unlock(pvt);
		}
		else
		{
			snprintf(buf, sizeof(buf), "Datacard %s not found.", datacard);
			astman_send_error(s, m, buf);
		}
	}
	else
	{
		astman_send_listack(s, m, "Datacard status list will follow", "start");

		AST_RWLIST_RDLOCK(&datacards);
		AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
		{
			pvt_lock(pvt);
			astman_append(s,
				"Event: DatacardStatusEntry\r\n"
				"%s"
				"Device: %s\r\n"
				"IMEI: %s\r\n"
				"IMSI: %s\r\n"
				"Number: %s\r\n"
				"Status: %s\r\n"
				"\r\n",
				idtext,
				pvt->id,
				pvt->imei,
				pvt->imsi,
				pvt->number,
				datacard_status_txt(pvt)
			);
			pvt_unlock(pvt);
			count++;
		}
		AST_RWLIST_UNLOCK(&datacards);

		astman_append(s,
			"Event: DatacardShowStatusComplete\r\n%s"
			"EventList: Complete\r\n"
			"ListItems: %zu\r\n"
			"\r\n",
			idtext, count
		);
	}

	return 0;
}

static int manager_action_send_sms(struct mansession *s, const struct message *m)
{
	return 0;
}

static int manager_action_send_ussd(struct mansession *s, const struct message *m)
{
	const char *datacard = astman_get_header(m, "Device");
	const char *ussd = astman_get_header(m, "USSD");
	const char *id = astman_get_header(m, "ActionID");

	char idtext[256] = "";
	struct datacard_pvt *pvt = NULL;
	char buf[256];

	if (ast_strlen_zero(datacard))
	{
		astman_send_error(s, m, "Datacard not specified");
		return 0;
	}

	if (ast_strlen_zero(ussd))
	{
		astman_send_error(s, m, "USSD not specified");
		return 0;
	}

	if (!ast_strlen_zero(id))
	{
		snprintf(idtext, sizeof(idtext), "ActionID: %s\r\n", id);
	}

	pvt = find_datacard(datacard);

	if (pvt)
	{
		pvt_lock(pvt);
		if (can_ussd(pvt))
		{
			if (at_send_cusd(pvt, ussd))
			{
				ast_log(LOG_ERROR, "[%s] Error sending USSD command\n", pvt->id);
				astman_send_error(s, m, "Datacard ussd failed");
			}
			else
			{
				astman_send_ack(s, m, "Datacard ussd successful");
			}
		}
		else
		{
			snprintf(buf, sizeof(buf), "Datacard %s status error", datacard);
			astman_send_error(s, m, buf);
		}
		pvt_unlock(pvt);
	}
	else
	{
		snprintf(buf, sizeof(buf), "Datacard %s not found", datacard);
		astman_send_error(s, m, buf);
	}

	return 0;
}

static int manager_action_reboot(struct mansession *s, const struct message *m)
{
	const char *datacard = astman_get_header(m, "Device");
	const char *id = astman_get_header(m, "ActionID");

	char idtext[256] = "";
	struct datacard_pvt *pvt = NULL;
	char buf[256];

	if (ast_strlen_zero(datacard))
	{
		astman_send_error(s, m, "Datacard not specified");
		return 0;
	}

	if (!ast_strlen_zero(id))
	{
		snprintf(idtext, sizeof(idtext), "ActionID: %s\r\n", id);
	}

	pvt = find_datacard(datacard);

	if (pvt)
	{
		pvt_lock(pvt);
		if (pvt->connected)
		{
			if (at_send_cfun_reboot(pvt))
			{
				ast_log(LOG_ERROR, "[%s] Error sending reboot command\n", pvt->id);
				astman_send_error(s, m, "Datacard reboot failed");
			}
			else
			{
				pvt->incoming = 1;
				manager_event_status(pvt);
				astman_send_ack(s, m, "Datacard rebooted successful");
			}
		}
		else
		{
			snprintf(buf, sizeof(buf), "Datacard %s status error", datacard);
			astman_send_error(s, m, buf);
		}
		pvt_unlock(pvt);
	}
	else
	{
		snprintf(buf, sizeof(buf), "Datacard %s not found", datacard);
		astman_send_error(s, m, buf);
	}

	return 0;
}

static void manager_event_new_ussd_base64(struct datacard_pvt *pvt, char *base64)
{
        manager_event(EVENT_FLAG_CALL, "DatacardNewUSSD",
                "Device: %s\r\n"
		"IMEI: %s\r\n"
		"IMSI: %s\r\n"
                "Base64: %s\r\n",
                pvt->id,
		pvt->imei,
		pvt->imsi,
                base64
        );
}

static void manager_event_new_sms_base64(struct datacard_pvt *pvt, char *from, char *base64)
{
	manager_event(EVENT_FLAG_CALL, "DatacardNewSMS",
		"Device: %s\r\n"
		"IMEI: %s\r\n"
		"IMSI: %s\r\n"
		"From: %s\r\n"
		"Base64: %s\r\n",
		pvt->id,
		pvt->imei,
		pvt->imsi,
		from,
		base64
	);
}

static void manager_event_status(struct datacard_pvt *pvt)
{
	manager_event(EVENT_FLAG_SYSTEM, "DatacardStatus",
		"Device: %s\r\n"
		"IMEI: %s\r\n"
		"IMSI: %s\r\n"
		"Number: %s\r\n"
		"Status: %s\r\n",
		pvt->id,
		pvt->imei,
		pvt->imsi,
		pvt->number,
		datacard_status_txt(pvt)
	);
}
