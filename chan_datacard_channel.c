/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static struct ast_channel *channel_new(struct datacard_pvt *pvt, int state, const char *cid_num, const char *cid_name, const char *ext, const char *ctx, const struct ast_assigned_ids *assignedids, const struct ast_channel *requestor)
{
	struct ast_channel *chan;

	chan = ast_channel_alloc(1, state, cid_num ? cid_num : pvt->number, cid_name ? cid_name : pvt->id, "", ext ? ext : "", ctx ? ctx : "datacard", assignedids, requestor, 0, "Datacard/%s-%d", pvt->id, (int) ast_random() & 0xffff);

	if (chan == NULL)
	{
		return chan;
	}

	ast_channel_stage_snapshot(chan);

	ast_channel_tech_set(chan, &datacard_tech);
	ast_channel_set_readformat(chan, ast_format_slin);
	ast_channel_set_writeformat(chan, ast_format_slin);
	ast_channel_nativeformats_set(chan, datacard_tech.capabilities);

	ast_channel_tech_pvt_set(chan, pvt);

	if (state == AST_STATE_RING)
	{
		ast_channel_rings_set(chan, 1);
	}

	if (!ast_strlen_zero(pvt->id))
	{
		pbx_builtin_setvar_helper(chan, "DATACARD", pvt->id);
	}
	if (!ast_strlen_zero(pvt->provider))
	{
		pbx_builtin_setvar_helper(chan, "PROVIDER", pvt->provider);
	}
	if (!ast_strlen_zero(pvt->imei))
	{
		pbx_builtin_setvar_helper(chan, "IMEI", pvt->imei);
	}
	if (!ast_strlen_zero(pvt->imsi))
	{
		pbx_builtin_setvar_helper(chan, "IMSI", pvt->imsi);
	}

	if (pvt->vars)
	{
		struct ast_variable *var = NULL;
		struct ast_str *str = NULL;

		if ((str = ast_str_create(32)))
		{
			for (var = pvt->vars; var; var = var->next)
			{
				ast_str_substitute_variables(&str, 0, chan, var->value);
				pbx_builtin_setvar_helper(chan, var->name, str->str);
			}
		}
	}

	if (!ast_strlen_zero(pvt->language))
	{
		ast_channel_language_set(chan, pvt->language);
	}

	ast_jb_configure(chan, &jbconf_global);

	if (pvt->audio_fd != -1)
	{
		ast_channel_set_fd(chan, 0, pvt->audio_fd);

		if ((pvt->audio_timer = ast_timer_open()))
		{
			ast_channel_set_fd(chan, 1, ast_timer_fd(pvt->audio_timer));
			rb_reset(&pvt->audio_write_rb);
		}
	}

	ast_dsp_digitreset(pvt->dsp);

	ast_module_ref(ast_module_info->self);

	ast_channel_stage_snapshot_done(chan);
	ast_channel_unlock(chan);

	return chan;
}

static struct ast_channel *channel_request(attribute_unused const char *type, struct ast_format_cap *cap, const struct ast_assigned_ids *assignedids, const struct ast_channel *requestor, const char *data, int *cause)
{
	char			*datacard = NULL;
	char			*number = NULL;
	struct datacard_pvt	*pvt = NULL;
	int			group;
	size_t			i;
	size_t			j;
	size_t			c;
	size_t			last_used;

	if (data == NULL)
	{
		ast_log (LOG_WARNING, "Channel requested with no data\n");
		*cause = AST_CAUSE_INCOMPATIBLE_DESTINATION;
		return NULL;
	}

	if (ast_format_cap_iscompatible_format(cap, ast_format_slin) == AST_FORMAT_CMP_NOT_EQUAL)
	{
		struct ast_str *codec_buf = ast_str_alloca(64);
		ast_log(LOG_WARNING, "Asked to get a channel of unsupported format '%s'\n", ast_format_cap_get_names(cap, &codec_buf));
		*cause = AST_CAUSE_FACILITY_NOT_IMPLEMENTED;
		return NULL;
	}

	datacard = ast_strdupa(data);
	number = strchr(datacard, '/');

	if (number == NULL)
	{
		ast_log(LOG_WARNING, "Can't determine destination\n");
		*cause = AST_CAUSE_INCOMPATIBLE_DESTINATION;
		return NULL;
	}

	*number = '\0'; number++;

	if (ast_strlen_zero(number))
	{
		ast_log (LOG_WARNING, "Empty destination\n");
		*cause = AST_CAUSE_INCOMPATIBLE_DESTINATION;
		return NULL;
	}

	AST_RWLIST_RDLOCK(&datacards);

	if (((datacard[0] == 'g') || (datacard[0] == 'G')) && ((datacard[1] >= '0') && (datacard[1] <= '9')))
	{
		errno = 0;
		group = (int) strtol(&datacard[1], (char **) NULL, 10);

		if (errno != EINVAL)
		{
			AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
			{
				pvt_lock(pvt);
				if (pvt->group == group && !pvt->owner && can_outgoing(pvt))
				{
					break;
				}
				pvt_unlock(pvt);
			}
		}
	}
	else if (((datacard[0] == 'r') || (datacard[0] == 'R')) && ((datacard[1] >= '0') && (datacard[1] <= '9')))
	{
		errno = 0;
		group = (int) strtol(&datacard[1], (char **) NULL, 10);

		if (errno != EINVAL)
		{
			ast_mutex_lock(&round_robin_mtx);

			/* Generate a list of all availible datacards */
			j = ARRAY_LEN(round_robin);
			c = 0; last_used = 0;
			AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
			{
				pvt_lock(pvt);
				if (pvt->group == group)
				{
					round_robin[c] = pvt;
					if (pvt->group_last_used == 1)
					{
						pvt->group_last_used = 0;
						last_used = c;
					}

					c++;

					if (c == j)
					{
						pvt_unlock(pvt);
						break;
					}
				}
				pvt_unlock(pvt);
			}

			/* Search for a availible datacard starting at the last used datacard */
			for (i = 0, j = last_used + 1; i < c; i++, j++)
			{
				if (j == c)
				{
					j = 0;
				}

				pvt = round_robin[j];

				pvt_lock(pvt);
				if (pvt->owner == NULL && can_outgoing(pvt))
				{
					pvt->group_last_used = 1;
					break;
				}
				pvt_unlock(pvt);
			}

			ast_mutex_unlock(&round_robin_mtx);
		}
	}
	else if (((datacard[0] == 's') || (datacard[0] == 'S')) && datacard[1] == ':')
	{
		ast_mutex_lock(&round_robin_mtx);

		/* Generate a list of all availible datacards */
		j = ARRAY_LEN(round_robin);
		c = 0; last_used = 0;
		i = strlen(&datacard[2]);
		AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
		{
			pvt_lock(pvt);
			if (!strncmp(pvt->imsi, &datacard[2], i))
			{
				round_robin[c] = pvt;
				if (pvt->sim_last_used == 1)
				{
					pvt->sim_last_used = 0;
					last_used = c;
				}

				c++;

				if (c == j)
				{
					pvt_unlock(pvt);
					break;
				}
			}
			pvt_unlock(pvt);
		}

		/* Search for a availible datacard starting at the last used datacard */
		for (i = 0, j = last_used + 1; i < c; i++, j++)
		{
			if (j == c)
			{
				j = 0;
			}

			pvt = round_robin[j];

			pvt_lock(pvt);
			if (pvt->owner == NULL && can_outgoing(pvt))
			{
				pvt->sim_last_used = 1;
				break;
			}
			pvt_unlock(pvt);
		}

		ast_mutex_unlock(&round_robin_mtx);
	}
	else if (((datacard[0] == 'i') || (datacard[0] == 'I')) && datacard[1] == ':')
	{
		AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
		{
			pvt_lock(pvt);
			if (!strcmp(pvt->imei, &datacard[2]))
			{
				break;
			}
			pvt_unlock(pvt);
		}
	}
	else
	{
		AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
		{
			pvt_lock(pvt);
			if (!strcmp(pvt->id, datacard))
			{
				break;
			}
			pvt_unlock(pvt);
		}
	}

	AST_RWLIST_UNLOCK(&datacards);

	if (pvt == NULL || pvt->owner || !can_outgoing(pvt))
	{
		if (pvt)
		{
			pvt_unlock(pvt);
		}
	
		ast_log(LOG_WARNING, "Request to call on datacard '%s' which can not make call at this moment\n", datacard);
		*cause = AST_CAUSE_NORMAL_CIRCUIT_CONGESTION;
		return NULL;
	}

	struct ast_channel *channel = pvt->owner = channel_new(pvt, AST_STATE_DOWN, NULL, NULL, number, NULL, assignedids, requestor);

	pvt_unlock(pvt);

	if (channel == NULL)
	{
		ast_log(LOG_WARNING, "Unable to allocate channel structure\n");
		*cause = AST_CAUSE_SWITCH_CONGESTION;
	}

	return channel;
}

static int channel_call(struct ast_channel *chan, const char *destination, attribute_unused int timeout)
{
	struct datacard_pvt *pvt = ast_channel_tech_pvt(chan);
	char *datacard = ast_strdupa(destination);
	char *number = strchr(datacard, '/');
	int clir = 0;

	if (number == NULL)
	{
		ast_log(LOG_WARNING, "Can't determine destination\n");
		return -1;
	}

	*number = '\0'; number++;

	if (ast_strlen_zero(number))
	{
		ast_log(LOG_WARNING, "Empty destination\n");
		return -1;
	}

	if ((ast_channel_state(chan) != AST_STATE_DOWN) && (ast_channel_state(chan) != AST_STATE_RESERVED))
	{
		ast_log(LOG_WARNING, "channel_call called on %s, neither down nor reserved\n", ast_channel_name(chan));
		return -1;
	}

	pvt_lock(pvt);

	if (!can_outgoing(pvt))
	{
		pvt_unlock(pvt);
		ast_log(LOG_NOTICE, "[%s] Datacard already in use\n", pvt->id);
		return -1;
	}

	ast_debug(1, "[%s] Calling %s on %s\n", pvt->id, destination, ast_channel_name(chan));

	if (pvt->usecallingpres)
	{
		if (pvt->callingpres < 0)
		{
			clir = ast_channel_connected(chan)->id.number.presentation;
		}
		else
		{
			clir = pvt->callingpres;
		}

		clir = get_at_clir_value(pvt->id, clir);

		if (at_send_clir(pvt, clir))
		{
			ast_log(LOG_ERROR, "[%s] Error sending AT+CLIR command\n", pvt->id);
		}
	}

	if (at_send_atd(pvt, number))
	{
		pvt_unlock(pvt);
		ast_log(LOG_ERROR, "[%s] Error sending ATD command\n", pvt->id);
		return -1;
	}

	pvt->outgoing = 1;
	pvt->needchup = 1;

	manager_event_status(pvt);

	pvt_unlock(pvt);

	return 0;
}

static int channel_hangup(struct ast_channel *chan)
{
	struct datacard_pvt *pvt = ast_channel_tech_pvt(chan);

	if (pvt == NULL)
	{
		ast_log(LOG_WARNING, "Asked to hangup channel not connected\n");
		return 0;
	}

	ast_debug(1, "[%s] Hangup channel %s\n", pvt->id, ast_channel_name(chan));

	pvt_lock(pvt);

	if (pvt->audio_timer)
	{
		ast_timer_close(pvt->audio_timer);
		pvt->audio_timer = NULL;
	}

	if (pvt->needchup)
	{
		if (at_send_chup(pvt))
		{
			ast_log(LOG_ERROR, "[%s] Error sending AT+CHUP command\n", pvt->id);
		}
		else
		{
			pvt->needchup = 0;
		}
	}

	pvt->owner = NULL;

	pvt_unlock(pvt);

	ast_channel_tech_pvt_set(chan, NULL);
	ast_module_unref(ast_module_info->self);
	ast_setstate(chan, AST_STATE_DOWN);

	return 0;
}

static int channel_answer(struct ast_channel *chan)
{
	struct datacard_pvt *pvt = ast_channel_tech_pvt(chan);

	pvt_lock(pvt);

	if (pvt->incoming)
	{
		if (at_send_ata(pvt))
		{
			ast_log(LOG_ERROR, "[%s] Error sending ATA command\n", pvt->id);
		}
	}

	pvt_unlock(pvt);

	return 0;
}

static int channel_digit_begin(struct ast_channel *chan, char digit)
{
	struct datacard_pvt *pvt = ast_channel_tech_pvt(chan);

	pvt_lock(pvt);

	if (at_send_dtmf(pvt, digit))
	{
		ast_log(LOG_ERROR, "[%s] Error sending DTMF %c\n", pvt->id, digit);
	}

	pvt_unlock(pvt);

	ast_debug(1, "[%s] Send DTMF %c\n", pvt->id, digit);

	return 0;
}

static int channel_digit_end(attribute_unused struct ast_channel *channel, attribute_unused char digit, attribute_unused unsigned int duration)
{
	return 0;
}

static struct ast_frame *channel_read(struct ast_channel *chan)
{
	struct datacard_pvt *pvt = ast_channel_tech_pvt(chan);
	struct ast_frame *f = &pvt->audio_read_frame;
	ssize_t res;

	while (ast_mutex_trylock(&pvt->lock))
	{
		CHANNEL_DEADLOCK_AVOIDANCE(chan);
	}

	memset(f, 0, sizeof(struct ast_frame));

	f->frametype = AST_FRAME_NULL;
	f->src = datacard_tech.type;

	if (pvt->owner == NULL || pvt->audio_fd == -1)
	{
		goto e_return;
	}

	if (pvt->audio_timer && ast_channel_fdno(chan) == 1)
	{
		ast_timer_ack(pvt->audio_timer, 1);
		channel_timing_write(pvt);
	}
	else
	{
		size_t count = 0;

		while ((res = read(pvt->audio_fd, pvt->audio_read_buf + AST_FRIENDLY_OFFSET, FRAME_SIZE)) < 0 && (errno == EINTR || errno == EAGAIN))
		{
			if (count++ > 10)
			{
				ast_debug(1, "[%s] Deadlock avoided for audio read!\n", pvt->id);
				goto e_return;
			}
		}

		if (res <= 0)
		{
			ast_debug(1, "[%s] Audio read() error: %s\n", pvt->id, strerror(errno));
			goto e_return;
		}

		f->frametype = AST_FRAME_VOICE;
		f->subclass.format = ao2_bump(ast_format_slin);
		f->samples   = res / 2;
		f->datalen   = res;
		f->data.ptr  = pvt->audio_read_buf + AST_FRIENDLY_OFFSET;
		f->offset    = AST_FRIENDLY_OFFSET;

#if __BYTE_ORDER == __BIG_ENDIAN
		ast_frame_byteswap_le(&f);
#endif

		if (pvt->dsp)
		{
			f = ast_dsp_process(chan, pvt->dsp, f);

			if (f->frametype == AST_FRAME_DTMF_END)
			{
				ast_debug(1, "[%s] Got DTMF char %c\n", pvt->id, f->subclass.integer);
			}
		}

		if (pvt->rxgain && f->frametype == AST_FRAME_VOICE)
		{
			if (ast_frame_adjust_volume(f, pvt->rxgain) == -1)
			{
				ast_debug (1, "[%s] Volume could not be adjusted!\n", pvt->id);
			}
		}
	}

e_return:
	pvt_unlock(pvt);

	return f;
}

static void channel_timing_write(struct datacard_pvt *pvt)
{
	size_t		used;
	int		iovcnt;
	struct iovec	iov[3];
	ssize_t		res;

	used = rb_used(&pvt->audio_write_rb);

	if (used >= FRAME_SIZE)
	{
		iovcnt = rb_read_n_iov(&pvt->audio_write_rb, iov, FRAME_SIZE);
		rb_read_upd(&pvt->audio_write_rb, FRAME_SIZE);
	}
	else if (used > 0)
	{
		iovcnt = rb_read_all_iov(&pvt->audio_write_rb, iov);
		rb_read_upd(&pvt->audio_write_rb, used);

		iov[iovcnt].iov_base	= silence_frame;
		iov[iovcnt].iov_len	= FRAME_SIZE - used;
		iovcnt++;
	}
	else
	{
		iov[0].iov_base		= silence_frame;
		iov[0].iov_len		= FRAME_SIZE;
		iovcnt			= 1;
	}

	size_t count = 0;

	while ((res = writev(pvt->audio_fd, iov, iovcnt)) < 0 && (errno == EINTR || errno == EAGAIN))
	{
		if (count++ > 10)
		{
			ast_debug(1, "[%s] Deadlock avoided for audio write!\n", pvt->id);
			return;
		}
	}

	if (res <= 0)
	{
		ast_debug(1, "[%s] writev() error: %s\n", pvt->id, strerror(errno));
	}
	else if (res != FRAME_SIZE)
	{
		ast_debug(1, "[%s] Partial audio write! (only %zd byte)\n", pvt->id, res);
	}
}

static int channel_write(struct ast_channel *chan, struct ast_frame *f)
{
	struct datacard_pvt *pvt = ast_channel_tech_pvt(chan);
	ssize_t res;

	while (ast_mutex_trylock(&pvt->lock))
	{
		CHANNEL_DEADLOCK_AVOIDANCE(chan);
	}

#ifdef __DEBUG__
	if (pvt->audio_fd == -1)
	{
		ast_debug(1, "[%s] audio_fd not ready\n", pvt->id);
	}
	else
#endif
	{
		if (pvt->txgain && f->datalen > 0)
		{
			if (ast_frame_adjust_volume(f, pvt->txgain) == -1)
			{
				ast_debug(1, "[%s] Volume could not be adjusted!\n", pvt->id);
			}
		}

#if __BYTE_ORDER == __BIG_ENDIAN
		ast_frame_byteswap_le(&f);
#endif

		if (pvt->audio_timer)
		{
			rb_write(&pvt->audio_write_rb, f->data.ptr, f->datalen);
		}
		else
		{
			int		iovcnt;
			struct iovec	iov[2];

			iov[0].iov_base	= f->data.ptr;
			iov[0].iov_len	= FRAME_SIZE;
			iovcnt		= 1;

			if (f->datalen < FRAME_SIZE)
			{
				iov[0].iov_len	= f->datalen;
				iov[1].iov_base	= silence_frame;
				iov[1].iov_len	= FRAME_SIZE - f->datalen;
				iovcnt		= 2;
			}

			size_t count = 0;

			while ((res = writev(pvt->audio_fd, iov, iovcnt)) < 0 && (errno == EINTR || errno == EAGAIN))
			{
				if (count++ > 10)
				{
					ast_debug(1, "[%s] Deadlock avoided for audio write!\n", pvt->id);
					goto e_return;
				}
			}

			if (res <= 0)
			{
				ast_debug(1, "[%s] writev() error: %s\n", pvt->id, strerror(errno));
			}
			else if (res != FRAME_SIZE)
			{
				ast_debug(1, "[%s] Partial audio write! (only %zd byte)\n", pvt->id, res);
			}
		}
	}

e_return:
	pvt_unlock(pvt);

	return 0;
}

static int channel_fixup(struct ast_channel *old_chan, struct ast_channel *new_chan)
{
	struct datacard_pvt *pvt = ast_channel_tech_pvt(new_chan);

	if (pvt == NULL)
	{
		ast_debug(1, "Channel fixup failed, no pvt on newchan\n");
		return -1;
	}

	pvt_lock(pvt);
	if (pvt->owner == old_chan)
	{
		pvt->owner = new_chan;
	}
	pvt_unlock(pvt);

	return 0;
}

static int channel_devicestate(const char *data)
{
	struct datacard_pvt *pvt;
	char *datacard = ast_strdupa(data ? data : "");
	int res = AST_DEVICE_INVALID;

	ast_debug(1, "[%s] Checking datacard state\n", datacard);

	pvt = find_datacard(datacard);

	if (pvt)
	{
		pvt_lock(pvt);
		if (pvt->connected)
		{
			if (pvt->incoming || pvt->outgoing || pvt->owner)
			{
				res = AST_DEVICE_INUSE;
			}
			else
			{
				res = AST_DEVICE_NOT_INUSE;
			}
		}
		pvt_unlock(pvt);
	}

	return res;
}

static int channel_indicate(struct ast_channel *chan, int condition, const void *data, attribute_unused size_t datalen)
{
	struct datacard_pvt *pvt = ast_channel_tech_pvt(chan);
	int res = 0;

	pvt_lock(pvt);

	ast_debug(1, "[%s] Requested indication %d\n", pvt->id, condition);

	switch (condition)
	{
		case AST_CONTROL_RINGING:
		case AST_CONTROL_BUSY:
		case AST_CONTROL_CONGESTION:
		case AST_CONTROL_PROGRESS:
		case AST_CONTROL_PROCEEDING:
		case AST_CONTROL_VIDUPDATE:
		case AST_CONTROL_SRCUPDATE:
		case AST_CONTROL_SRCCHANGE:
		case AST_CONTROL_CONNECTED_LINE:
		case AST_CONTROL_REDIRECTING:
		case AST_CONTROL_INCOMPLETE:
		case AST_CONTROL_UPDATE_RTP_PEER:
		case AST_CONTROL_PVT_CAUSE_CODE:
			break;

		case -1:
			res = -1;
			break;

		case AST_CONTROL_HOLD:
			ast_moh_start(chan, data, NULL);
			break;

		case AST_CONTROL_UNHOLD:
			ast_moh_stop(chan);
			break;

		default:
			ast_log(LOG_WARNING, "[%s] Don't know how to indicate condition %d on %s\n", pvt->id, condition, ast_channel_name(chan));
			res = -1;
			break;
	}

	pvt_unlock(pvt);

	return res;
}

static int channel_queue_control(struct datacard_pvt *pvt, enum ast_control_frame_type control)
{
	while (pvt->owner && ast_channel_trylock(pvt->owner))
	{
		DEADLOCK_AVOIDANCE(&pvt->lock);
	}

	ast_queue_control(pvt->owner, control);
	ast_channel_unlock(pvt->owner);

	return 0;
}

static int channel_queue_hangup(struct datacard_pvt *pvt, int cause)
{
	int res = 0;

	while (pvt->owner && ast_channel_trylock(pvt->owner))
	{
		DEADLOCK_AVOIDANCE(&pvt->lock);
	}

	if (cause)
	{
		res = ast_queue_hangup_with_cause(pvt->owner, cause);
	}
	else
	{
		res = ast_queue_hangup(pvt->owner);
	}

	ast_channel_unlock(pvt->owner);

	return res;
}

#ifdef __ALLOW_LOCAL_CHANNELS__
static struct ast_channel *channel_local_request(struct datacard_pvt *pvt, void *data, const char *cid_name, const char *cid_num)
{
	struct ast_channel *chan;
	int cause = 0;

	chan = ast_request("Local", AST_FORMAT_TEXT_MASK, NULL, data, &cause);
	if (chan == NULL)
	{
		ast_log(LOG_NOTICE, "Unable to request channel Local/%s\n", (char *) data);
		return chan;
	}

	ast_set_callerid(chan, cid_num, cid_name, cid_num);

	if (!ast_strlen_zero(pvt->id))
	{
		pbx_builtin_setvar_helper(chan, "DATACARD", pvt->id);
	}
	if (!ast_strlen_zero(pvt->provider))
	{
		pbx_builtin_setvar_helper(chan, "PROVIDER", pvt->provider);
	}
	if (!ast_strlen_zero(pvt->imei))
	{
		pbx_builtin_setvar_helper(chan, "IMEI", pvt->imei);
	}
	if (!ast_strlen_zero(pvt->imsi))
	{
		pbx_builtin_setvar_helper(chan, "IMSI", pvt->imsi);
	}

	if (pvt->vars)
	{
		struct ast_variable* var = NULL;
		struct ast_str* str = NULL;

		if ((str = ast_str_create(32)))
		{
			for (var = pvt->vars; var; var = var->next)
			{
				ast_str_substitute_variables(&str, 0, chan, var->value);
				pbx_builtin_setvar_helper(chan, var->name, str->str);
			}
		}
	}

	if (!ast_strlen_zero(pvt->language))
	{
		ast_string_field_set(chan, language, pvt->language);
	}

	return chan;
}
#endif /* __ALLOW_LOCAL_CHANNELS__ */
