/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static int at_wait_read(const char *datacard, int *fds, int nfds, int timeout)
{
	int i, res;
	struct pollfd pollfds[nfds];

	for (i = 0; i < nfds; i++)
	{
		pollfds[i].fd = fds[i];
		pollfds[i].events = POLLIN;
	}

	res = ast_poll(pollfds, nfds, timeout);

	if (res > 0)
	{
		for (i = 0; i < nfds; i++)
		{
			if (pollfds[i].revents & POLLIN)
			{
				return i + 1;
			}
		}
	}

	if (res < 0 && errno != EINTR && errno != EAGAIN)
	{
		ast_log(LOG_WARNING, "[%s] poll() error: %s\n", datacard, strerror(errno));
	}

	return 0;
}

static ssize_t at_read(const char *datacard, const char *tty, int fd, char *buf, size_t len)
{
	ssize_t	n;

	if ((n = read(fd, buf, len)) > 0)
	{
		ast_debug(5, "[%s] %s [%.*s]\n", datacard, tty, (int) n, buf);
		return n;
	}

	if (n < 0)
	{
		if (errno == EINTR || errno == EAGAIN)
		{
			return 0;
		}
	
		ast_debug(1, "[%s] read() error: %s\n", datacard, strerror(errno));
	}

	return -1;
}
