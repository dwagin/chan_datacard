/*
 * Copyright (C) 2014
 *
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

#ifndef __AT_BUFFER_H__
#define __AT_BUFFER_H__

#include <time.h>

typedef enum {
	CMD_UNKNOWN = 0,

	CMD_AT,
	CMD_AT_CMEE,
	CMD_AT_E,
	CMD_AT_Z,

	CMD_AT_CGMI,
	CMD_AT_CGMM,
	CMD_AT_CGMR,
	CMD_AT_CGSN,

	CMD_AT_U2DIAG,

	CMD_AT_CIMI,
	CMD_AT_CPIN_TEST,
	CMD_AT_CNUM,
	CMD_AT_CLIP,

	CMD_AT_COPS_INIT,
	CMD_AT_COPS,
	CMD_AT_CREG_INIT,
	CMD_AT_CREG,
	CMD_AT_STSF,

	CMD_AT_CVOICE,
	CMD_AT_CSSN,

	CMD_AT_CMGF,
	CMD_AT_CSCS,
	CMD_AT_CPMS,
	CMD_AT_CNMI,

	CMD_AT_CURC,

	CMD_AT_CSQ,

	CMD_AT_A,
	CMD_AT_D,
	CMD_AT_CLIR,
	CMD_AT_DDSETEX,
	CMD_AT_CLVL,
	CMD_AT_CHUP,
	CMD_AT_DTMF,

	CMD_AT_CUSD,

	CMD_AT_CMGR,
	CMD_AT_CMGD,
	CMD_AT_CMGS,
	CMD_AT_SMS_TEXT,

	CMD_AT_CCWA,
	CMD_AT_CFUN,
	CMD_AT_CPIN,
	CMD_AT_CLCK,

} at_command_t;

typedef enum {
	RES_PARSE_ERROR = -1,
	RES_UNKNOWN = 0,

	RES_OK,
	RES_COPS,
	RES_CREG,
	RES_ERROR,

	RES_CPIN,
	RES_CNUM,
	RES_CVOICE,
	RES_CPMS,
	RES_CSQ,

	RES_BUSY,
	RES_NO_CARRIER,
	RES_NO_DIALTONE,

	RES_CMGR,
	RES_SMS_PROMPT,

} at_response_t;

typedef enum {
	EVENT_PARSE_ERROR = -1,
	EVENT_UNKNOWN = 0,

	EVENT_BOOT,
	EVENT_RSSI,
	EVENT_MODE,

	EVENT_CEND,
	EVENT_ORIG,
	EVENT_CSSI,
	EVENT_CONF,
	EVENT_CONN,

	EVENT_CLIP,
	EVENT_RING,

	EVENT_CREG,
	EVENT_SRVST,

	EVENT_CUSD,
	EVENT_CMTI,
	EVENT_SMMEMFULL,

	EVENT_CSSU,

} at_event_t;

struct at_entry
{
	struct timeval	timeout;
	at_command_t	command;
	at_response_t	response;
	char		buf[512];
	size_t		len;
};

struct at_buffer
{
	struct at_entry	*buffer;
	size_t		size;
	size_t		used;
	size_t		read;
	size_t		write;
};

static inline void	at_buf_init		(struct at_buffer *, struct at_entry *, size_t);
static inline void	at_buf_reset		(struct at_buffer *);

static inline size_t	at_buf_used		(struct at_buffer *);
static inline size_t	at_buf_free		(struct at_buffer *);

static struct at_entry	*at_buf_read		(struct at_buffer *);
static void		at_buf_read_finish	(struct at_buffer *);

static struct at_entry	*at_buf_write		(struct at_buffer *);
static void		at_buf_write_finish	(struct at_buffer *);

#endif /* __AT_BUFFER_H__ */
