/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static int at_event(struct datacard_pvt *pvt, char *str, size_t len)
{
	static struct
	{
		at_event_t	e;
		const char	*str;
		size_t		len;
	}
	events_list[] = {

		{ EVENT_BOOT,		STR_AND_LEN("^BOOT:"),		}, // 5115
		{ EVENT_RSSI,		STR_AND_LEN("^RSSI:"),		}, // 880
		{ EVENT_MODE,		STR_AND_LEN("^MODE:"),		}, // 656

		{ EVENT_CEND,		STR_AND_LEN("^CEND:"),		}, // 425
		{ EVENT_ORIG,		STR_AND_LEN("^ORIG:"),		}, // 408
		{ EVENT_CSSI,		STR_AND_LEN("+CSSI:"),		}, // 404
		{ EVENT_CONF,		STR_AND_LEN("^CONF:"),		}, // 404
		{ EVENT_CONN,		STR_AND_LEN("^CONN:"),		}, // 332

		{ EVENT_CLIP,		STR_AND_LEN("+CLIP:"),		},
		{ EVENT_RING,		STR_AND_LEN("RING\r"),		},

		{ EVENT_CREG,		STR_AND_LEN("+CREG:"),		}, // 56
		{ EVENT_SRVST,		STR_AND_LEN("^SRVST:"),		}, // 35

		{ EVENT_CUSD,		STR_AND_LEN("+CUSD:"),		},
		{ EVENT_CMTI,		STR_AND_LEN("+CMTI:"),		},
		{ EVENT_SMMEMFULL,	STR_AND_LEN("^SMMEMFULL:"),	},

		{ EVENT_CSSU,		STR_AND_LEN("+CSSU:"),		},

//		{ EVENT_PARSE_ERROR,	STR_AND_LEN("\r"),		},
	};

	size_t i;
	at_event_t at_event = EVENT_UNKNOWN;

	ast_debug(1, "[%s] Event: [%.*s]\n", pvt->id, (int) len, str);

	for (i = 0; i < ARRAY_LEN(events_list); i++)
	{
		if (!strncmp(str, events_list[i].str, events_list[i].len))
		{
			at_event = events_list[i].e;
			break;
		}
	}

	switch (at_event)
	{
		case EVENT_RSSI:
			return at_event_rssi(pvt, str);

		case EVENT_MODE:
			return at_event_mode(pvt, str);

		case EVENT_CEND:
			return at_event_cend(pvt, str);

		case EVENT_CONF:
			return at_event_conf(pvt, str);

		case EVENT_CONN:
			return at_event_conn(pvt);

		case EVENT_CLIP:
			return at_event_clip(pvt, str, len);

		case EVENT_CREG:
			return at_response_creg(pvt, str, len);

		case EVENT_CUSD:
			return at_event_cusd(pvt, str, len);

		case EVENT_CMTI:
			return at_event_cmti(pvt, str, len);

		case EVENT_SMMEMFULL:
			return at_event_smmemfull(pvt);

		case EVENT_UNKNOWN:
			trimnr(str, &len, "\r\n");
			ast_debug(1, "[%s] Ignoring unknown event: [%.*s]\n", pvt->id, (int) len, str);
			break;

		case EVENT_PARSE_ERROR:
			ast_log(LOG_WARNING, "[%s] Strange event: [%.*s]\n", pvt->id, (int) len, str);
			return -1;

		default:
			break;
	}

	return 0;
}

static int at_event_rssi(struct datacard_pvt *pvt, char *str)
{
	pvt->rssi = -1;

	/*
	 * parse RSSI info in the following format:
	 * ^RSSI:<rssi>
	 */

	if (!sscanf(str, "^RSSI:%d", &pvt->rssi))
	{
		ast_debug(2, "[%s] Error parsing ^RSSI\n", pvt->id);
		return -1;
	}

	return 0;
}

static int at_event_mode(struct datacard_pvt *pvt, char *str)
{
	pvt->linkmode = -1;
	pvt->linksubmode = -1;

	/*
	 * parse RSSI info in the following format:
	 * ^MODE:<mode>,<submode>
	 */

	if (!sscanf(str, "^MODE:%d,%d", &pvt->linkmode, &pvt->linksubmode))
	{
		ast_debug(1, "[%s] Error parsing ^MODE\n", pvt->id);
		return -1;
	}

	return 0;
}

static int at_event_cend(struct datacard_pvt *pvt, char *str)
{
	int index = 0;
	int duration = 0;
	int end_status = 0;
	int cc_cause = 0;

	/*
	 * parse CEND info in the following format:
	 * ^CEND:<index>,<duration>,<end_status>[,<cc_cause>]
	 */

	if (!sscanf(str, "^CEND:%d,%d,%d,%d", &index, &duration, &end_status, &cc_cause))
	{
		ast_debug(1, "[%s] Error parsing ^CEND\n", pvt->id);
	}

	ast_debug(1, "[%s] CEND: index:      %d\n", pvt->id, index);
	ast_debug(1, "[%s] CEND: duration:   %d\n", pvt->id, duration);
	ast_debug(1, "[%s] CEND: end_status: %d\n", pvt->id, end_status);
	ast_debug(1, "[%s] CEND: cc_cause:   %d\n", pvt->id, cc_cause);

	ast_debug(1, "[%s] Line disconnected\n", pvt->id);

	pvt->needchup = 0;

	if (pvt->owner)
	{
		ast_debug(1, "[%s] hanging up owner\n", pvt->id);

		if (channel_queue_hangup(pvt, cc_cause))
		{
			ast_log(LOG_ERROR, "[%s] Error queueing hangup...\n", pvt->id);
			return -1;
		}
	}

	pvt->incoming = 0;
	pvt->outgoing = 0;

	pvt->ringing = 0;
	pvt->talking = 0;

	manager_event_status(pvt);

	return 0;
}

static int at_event_conf(struct datacard_pvt *pvt, char *str)
{
	int index = 1;

	pvt->ringing = 1;

	channel_queue_control(pvt, AST_CONTROL_PROGRESS);

	if (at_send_ddsetex(pvt))
	{
		ast_log(LOG_ERROR, "[%s] Error sending AT^DDSETEX\n", pvt->id);
		return -1;
	}

	if (pvt->audio_timer)
	{
		ast_timer_set_rate(pvt->audio_timer, 50);
	}

	/*
	 * parse CONF info in the following format:
	 * ^CONF:<index>
	 */

	if (!sscanf(str, "^CONF:%d", &index))
	{
		ast_debug(1, "[%s] Error parsing ^CONF\n", pvt->id);
	}

	ast_debug(1, "[%s] Call started (index=%d)\n", pvt->id, index);

	return 0;
}

static int at_event_conn(struct datacard_pvt *pvt)
{
	pvt->ringing = 0;
	pvt->talking = 1;

	if (pvt->outgoing)
	{
		ast_debug(1, "[%s] Remote end answered\n", pvt->id);
		channel_queue_control(pvt, AST_CONTROL_ANSWER);
	}
	else if (pvt->incoming)
	{

		ast_setstate(pvt->owner, AST_STATE_UP);
	}

	return 0;
}

static int at_event_clip(struct datacard_pvt *pvt, char *str, size_t len)
{
	char *clip;

	if (pvt->owner == NULL && can_incoming(pvt))
	{
		pvt->incoming = 1;
		pvt->ringing  = 1;

		if (!(clip = at_parse_clip(str, len)))
		{
			ast_log(LOG_ERROR, "[%s] Error parsing +CLIP\n", pvt->id);
		}

		pvt->owner = channel_new(pvt, AST_STATE_RING, clip, NULL, pvt->number ? pvt->number : "s", pvt->context, NULL, NULL);

		if (pvt->owner == NULL)
		{
			ast_log(LOG_ERROR, "[%s] Unable to allocate channel for incoming call\n", pvt->id);

			if (at_send_chup(pvt))
			{
				ast_log(LOG_ERROR, "[%s] Error sending AT+CHUP command\n", pvt->id);
			}

			return -1;
		}

		pvt->needchup = 1;

		if (ast_pbx_start(pvt->owner))
		{
			ast_log(LOG_ERROR, "[%s] Unable to start pbx on incoming call\n", pvt->id);
			ast_channel_hangupcause_set(pvt->owner, AST_CAUSE_SWITCH_CONGESTION);
			ast_hangup(pvt->owner);
			return -1;
		}

		manager_event_status(pvt);
	}

	return 0;
}

static int at_event_cusd(struct datacard_pvt *pvt, char *str, size_t len)
{
	ssize_t res;
	int type;
	int dcs;
	char *text;
	char text_utf8[1024];
	char text_base64[2048];

	if (at_parse_cusd(str, len, &type, &text, &dcs))
	{
		ast_log(LOG_ERROR, "[%s] Error parsing +CUSD\n", pvt->id);
		return 0;
	}

	if (pvt->ussd_use_ucs2_decoding)
	{
		if ((res = conv_ucs2_8bit_hexstr_to_utf8(text, strlen(text), text_utf8, sizeof(text_utf8))) > 0)
		{
			text = text_utf8;
		}
		else
		{
			ast_log(LOG_ERROR, "[%s] Error parsing +CUSD, convert hexstring to UTF-8: %s\n", pvt->id, text);
			return -1;
		}
	}
	else
	{
		switch (parse_dcs(dcs))
		{
			case ALPHABET_GSM:
				res = conv_latin1_7bit_hexstr_to_utf8(text, strlen(text), text_utf8, sizeof(text_utf8));
				break;

			case ALPHABET_8BIT:
				res = conv_latin1_8bit_hexstr_to_utf8(text, strlen(text), text_utf8, sizeof(text_utf8));
				break;

			case ALPHABET_UCS2:
				res = conv_ucs2_8bit_hexstr_to_utf8(text, strlen(text), text_utf8, sizeof(text_utf8));
				break;

			default:
				ast_log(LOG_ERROR, "[%s] Error parsing +CUSD, unknown alphabet (dcs = %d)\n", pvt->id, dcs);
				return -1;
		}


		if (res > 0)
		{
			text = text_utf8;
		}
		else
		{
			ast_log(LOG_ERROR, "[%s] Error parsing +CUSD, convert hexstring to UTF-8: %s\n", pvt->id, text);
			return -1;
		}
	}

	ast_verb(1, "[%s] Got USSD message: [%s]\n", pvt->id, text);
	ast_base64encode(text_base64, (unsigned char *) text, strlen(text), sizeof(text_base64));

	manager_event_new_ussd_base64(pvt, text_base64);

#ifdef __ALLOW_LOCAL_CHANNELS__
	struct ast_channel *channel;
	char buf[128];

	snprintf(buf, sizeof(buf), "ussd@%s", pvt->context);

	if (channel = channel_local_request(pvt, buf, pvt->id, "ussd"))
	{
		pbx_builtin_setvar_helper(channel, "USSD_BASE64", text_base64);

		if (ast_pbx_start(channel))
		{
			ast_log(LOG_ERROR, "[%s] Unable to start pbx on incoming ussd\n", pvt->id);
			ast_hangup(channel);
		}
	}
#endif /* __ALLOW_LOCAL_CHANNELS__ */

	return 0;
}

static int at_event_cmti(struct datacard_pvt *pvt, char *str, size_t len)
{
	if (pvt->initialized && pvt->has_sms)
	{
		int index;

		if (!sscanf(str, "+CMTI: %*[^,],%d", &index))
		{
			ast_debug(1, "[%s] Error parsing +CMTI\n", pvt->id);
			return 0;
		}

		ast_debug(1, "[%s] Incoming SMS message\n", pvt->id);

		if (at_send_cmgr(pvt, index) || at_send_cmgd(pvt, index, 0))
		{
			ast_log(LOG_ERROR, "[%s] Error read SMS (index=%d)\n", pvt->id, index);
			return -1;
		}

		pvt->incoming_sms = 1;
	}

	return 0;
}

static int at_event_smmemfull(struct datacard_pvt *pvt)
{
	ast_log(LOG_ERROR, "[%s] SMS storage is full\n", pvt->id);
	return 0;
}
