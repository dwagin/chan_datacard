/*
 * Copyright (C) 2009
 *
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static inline void rb_init(struct ringbuffer *rb, void *buf, size_t size)
{
	rb->buffer = buf;
	rb->size   = size;
	rb->used   = 0;
	rb->read   = 0;
	rb->write  = 0;
}

static inline void rb_reset(struct ringbuffer *rb)
{
	rb->used   = 0;
	rb->read   = 0;
	rb->write  = 0;
}

static inline size_t rb_used(struct ringbuffer *rb)
{
	return rb->used;
}

static inline size_t rb_free(struct ringbuffer *rb)
{
	return rb->size - rb->used;
}

// ============================ READ ============================= 

static int rb_read_all_iov(struct ringbuffer *rb, struct iovec *iov)
{
	if (rb->used > 0)
	{
		if ((rb->read + rb->used) > rb->size)
		{
			iov[0].iov_base = rb->buffer + rb->read;
			iov[0].iov_len  = rb->size - rb->read;
			iov[1].iov_base = rb->buffer;
			iov[1].iov_len  = rb->used - iov[0].iov_len;

			return 2;
		}
		else
		{
			iov[0].iov_base = rb->buffer + rb->read;
			iov[0].iov_len  = rb->used;
			iov[1].iov_len  = 0;

			return 1;
		}
	}

	return 0;
}

static int rb_read_n_iov(struct ringbuffer *rb, struct iovec *iov, size_t len)
{
	if (rb->used >= len && len > 0)
	{
		if ((rb->read + len) > rb->size)
		{
			iov[0].iov_base = rb->buffer + rb->read;
			iov[0].iov_len  = rb->size - rb->read;
			iov[1].iov_base = rb->buffer;
			iov[1].iov_len  = len - iov[0].iov_len;

			return 2;
		}
		else
		{
			iov[0].iov_base = rb->buffer + rb->read;
			iov[0].iov_len  = len;
			iov[1].iov_len  = 0;

			return 1;
		}
	}

	return 0;
}

static size_t rb_read_upd(struct ringbuffer *rb, size_t len)
{
	size_t s;

	if (rb->used < len)
	{
		len = rb->used;
	}

	if (len > 0)
	{
		rb->used -= len;

		if (rb->used == 0)
		{
			rb->read  = 0;
			rb->write = 0;
		}
		else
		{
			s = rb->read + len;

			if (s >= rb->size)
			{
				rb->read = s - rb->size;
			}
			else
			{
				rb->read = s;
			}
		}
	}

	return len;
}

// ============================ WRITE ============================ 

static size_t rb_write(struct ringbuffer *rb, void *buf, size_t len)
{
	size_t	free;
	size_t	s;

	free = rb_free(rb);
	if (free < len)
	{
		len = free;
	}

	if (len > 0)
	{
		s = rb->write + len;

		if (s > rb->size)
		{
			memmove(rb->buffer + rb->write, buf, rb->size - rb->write);
			memmove(rb->buffer, buf + rb->size - rb->write, s - rb->size);

			rb->write = s - rb->size;
		}
		else
		{
			memmove(rb->buffer + rb->write, buf, len);

			if (s == rb->size)
			{
				rb->write = 0;
			}
			else
			{
				rb->write = s;
			}
		}

		rb->used += len;
	}

	return len;
}
