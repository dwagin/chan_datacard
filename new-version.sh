#!/bin/sh

DEFAULT="UKNOWN"

if [ -f chan_datacard_version.h ]; then
	git --version >/dev/null 2>&1 || \
	{
		echo "git not installed, use old chan_datacard_version.h"
		exit 0
	}
fi

if [ -d .git ]; then
	VER=`git describe --tags 2>/dev/null`
	DIFF=`git diff-index --name-only HEAD --`

	[ -z "${VER}" ] && VER=${DEFAULT}
	[ -z "${DIFF}" ] || VER="${VER}-mod"

else
	VER=${DEFAULT}
fi


cat << EOF > chan_datacard_version.h
#define DATACARD_VERSION "${VER}"
EOF
