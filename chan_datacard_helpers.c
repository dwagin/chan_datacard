/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static int tty_open(const char *dev)
{
	int		fd;
	struct termios	attr;

	fd = open(dev, O_RDWR | O_NOCTTY);

	if (fd < 0)
	{
		ast_debug(1, "Error open(%s): %s\n", dev, strerror(errno));
		return -1;
	}

	if (tcgetattr(fd, &attr))
	{
		tty_close(&fd);
		ast_log(LOG_WARNING, "tcgetattr() failed '%s'\n", dev);
		return -1;
	}

	attr.c_cflag = B115200 | CS8 | CREAD | CRTSCTS;
	attr.c_iflag = 0;
	attr.c_oflag = 0;
	attr.c_lflag = 0;
	attr.c_cc[VMIN] = 1;
	attr.c_cc[VTIME] = 0;

	if (tcsetattr(fd, TCSAFLUSH, &attr))
	{
		tty_close(&fd);
		ast_log(LOG_WARNING, "tcsetattr() failed '%s'\n", dev);
		return -1;
	}

	return fd;
}

static void tty_close(int *sock)
{
	if (*sock < 0)
	{
		return;
	}

	do
	{
		if (close(*sock) < 0)
		{
			if (errno == EINTR)
			{
				continue;
			}

			ast_debug(1, "Error close(%d): %s\n", *sock, strerror(errno));
		}

		*sock = -1;
	}
	while (0);
}

static int tty_status(int fd)
{
	struct termios t;

	if (fd < 0)
	{
		return -1;
	}

	return tcgetattr(fd, &t);
}

static struct ast_variable *parse_setvar(const char *buf, struct ast_variable *list)
{
	struct ast_variable	*var = NULL;
	char			*varname = ast_strdupa(buf);
	char			*varval = NULL;

	if ((varval = strchr(varname, '=')))
	{
		*varval++ = '\0';

		if ((var = ast_variable_new(varname, varval, "")))
		{
			var->next = list;
			list = var;
		}
	}

	return list;
}

static struct datacard_pvt *find_datacard(const char *name)
{
	struct datacard_pvt *pvt = NULL;

	AST_RWLIST_RDLOCK(&datacards);
	AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
	{
		if (!strcmp(pvt->id, name))
		{
			break;
		}
	}
	AST_RWLIST_UNLOCK(&datacards);

	return pvt;
}

static int can_incoming(struct datacard_pvt *pvt)
{
	if (pvt->initialized && pvt->has_voice && !pvt->offline && !pvt->incoming && !pvt->outgoing)
	{
		return 1;
	}

	return 0;
}

static int can_outgoing(struct datacard_pvt *pvt)
{
	if (pvt->connected && pvt->initialized && pvt->gsm_registered && pvt->has_voice && !pvt->offline && !pvt->incoming && !pvt->outgoing)
	{
		return 1;
	}

	return 0;
}

static int can_ussd(struct datacard_pvt *pvt)
{
	if (pvt->connected && pvt->initialized && pvt->gsm_registered && !pvt->incoming && !pvt->outgoing)
	{
		return 1;
	}

	return 0;
}

static int can_sms(struct datacard_pvt *pvt)
{
	if (pvt->connected && pvt->initialized && pvt->gsm_registered && pvt->has_sms && !pvt->incoming && !pvt->outgoing)
	{
		return 1;
	}

	return 0;
}

static const char *datacard_gsm_status_txt(struct datacard_pvt *pvt)
{
	return
		(pvt->gsm_status == 0) ? "Not registered, not searching" :
		(pvt->gsm_status == 1) ? "Registered, home network" :
		(pvt->gsm_status == 2) ? "Not registered, but searching" :
		(pvt->gsm_status == 3) ? "Denied registration" :
		(pvt->gsm_status == 5) ? "Registered, roaming" : "Unknown";
}

static const char *datacard_status_txt(struct datacard_pvt *pvt)
{
	return
		(!pvt->connected) ? "Not connected" :
		(!pvt->initialized) ? "Not initialized" :
		(pvt->gsm_status == 3) ? "GSM denied registration" :
		(!pvt->gsm_registered) ? "GSM not registered" :
		(pvt->outgoing || pvt->incoming) ? "Busy" :
		(pvt->outgoing_sms || pvt->incoming_sms) ? "SMS" :
		(pvt->offline) ? "Offline" : "Free";
}

static char *complete_datacard(attribute_unused const char *line, const char *word, attribute_unused int pos, int state, attribute_unused int flags)
{
	struct datacard_pvt *pvt;
	char *res = NULL;
	int which = 0;
	int wordlen = strlen(word);

	AST_RWLIST_RDLOCK(&datacards);
	AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
	{
		if (!strncasecmp(pvt->id, word, wordlen) && ++which > state)
		{
			res = ast_strdup(pvt->id);
			break;
		}
	}
	AST_RWLIST_UNLOCK(&datacards);

	return res;
}

static int get_at_clir_value(const char *datacard, int clir)
{
	int res = 0;

	switch (clir)
	{
		case AST_PRES_ALLOWED_NETWORK_NUMBER:
		case AST_PRES_ALLOWED_USER_NUMBER_FAILED_SCREEN:
		case AST_PRES_ALLOWED_USER_NUMBER_NOT_SCREENED:
		case AST_PRES_ALLOWED_USER_NUMBER_PASSED_SCREEN:
		case AST_PRES_NUMBER_NOT_AVAILABLE:
			ast_debug(2, "[%s] callingpres: %s\n", datacard, ast_describe_caller_presentation(clir));
			res = 2;
			break;

		case AST_PRES_PROHIB_NETWORK_NUMBER:
		case AST_PRES_PROHIB_USER_NUMBER_FAILED_SCREEN:
		case AST_PRES_PROHIB_USER_NUMBER_NOT_SCREENED:
		case AST_PRES_PROHIB_USER_NUMBER_PASSED_SCREEN:
			ast_debug(2, "[%s] callingpres: %s\n", datacard, ast_describe_caller_presentation(clir));
			res = 1;
			break;

		default:
			ast_log(LOG_WARNING, "[%s] Unsupported callingpres: %d\n", datacard, clir);
			if ((clir & AST_PRES_RESTRICTION) != AST_PRES_ALLOWED)
			{
				res = 0;
			}
			else
			{
				res = 2;
			}

			break;
	}

	return res;
}

static char *rssi2dBm(int rssi, char *buf, size_t len)
{
	if (rssi == 99 || rssi > 31 || rssi < 0)
	{
		snprintf(buf, len, "??? dBm (Unknown)");
	}
	else
	{
		int dbm = -113 + 2 * rssi;

		if (dbm <= -95)
		{
			snprintf(buf, len, "%d dBm (Marginal)", dbm);
		}
		else if (dbm <= -85)
		{
			snprintf(buf, len, "%d dBm (Workable)", dbm);
		}
		else if (dbm <= -75)
		{
			snprintf(buf, len, "%d dBm (Good)", dbm);
		}
		else
		{
			snprintf(buf, len, "%d dBm (Excellent)", dbm);
		}
	}

	return buf;
}

void trimnr(char *str, size_t *len, const char *chs)
{
	char *s;

	s = str + *len - 1;

	while ((s >= str) && strchr(chs, *s))
	{
		*s = '\0'; s--;
	}

	*len = s - str + 1;
}

static int parse_dcs(int dcs)
{
	switch ((dcs & 0xF0))
	{
		case 0x00:
			return ALPHABET_GSM;

		case 0x10:
			switch ((dcs & 0x0F))
			{
				case 0x00: return ALPHABET_GSM;
				case 0x01: return ALPHABET_UCS2;
				default:   return ALPHABET_RESERVED;
			}
			break;

		case 0x20:
		case 0x30:
			return ALPHABET_GSM;

		case 0x40:
		case 0x50:
		case 0x60:
		case 0x70:
			switch ((dcs & 0x0C))
			{
				case 0x00: return ALPHABET_GSM;
				case 0x04: return ALPHABET_8BIT;
				case 0x08: return ALPHABET_UCS2;
				case 0x0C: return ALPHABET_RESERVED;
			}
			break;
	}

	return ALPHABET_UNKNOWN;
}
