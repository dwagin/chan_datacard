/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static ssize_t conv_encoding			(const char *, size_t, char *, size_t, char *, char *);
static ssize_t conv_char_to_8bit_hexstr		(const char *, size_t, char *, size_t);
static ssize_t conv_char_to_7bit_hexstr		(const char *, size_t, char *, size_t);
static ssize_t conv_8bit_hexstr_to_char		(const char *, size_t, char *, size_t);
static ssize_t conv_7bit_hexstr_to_char		(const char *, size_t, char *, size_t);
static ssize_t conv_utf8_to_ucs2_8bit_hexstr	(const char *, size_t, char *, size_t);
static ssize_t conv_utf8_to_latin1_7bit_hexstr	(const char *, size_t, char *, size_t);
static ssize_t conv_ucs2_8bit_hexstr_to_utf8	(const char *, size_t, char *, size_t);
static ssize_t conv_latin1_8bit_hexstr_to_utf8	(const char *, size_t, char *, size_t);
static ssize_t conv_latin1_7bit_hexstr_to_utf8	(const char *, size_t, char *, size_t);
