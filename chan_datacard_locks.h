/*
 * Copyright (C) 2014
 *
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

/*
#ifdef __DEBUG__

#define pvt_lock(pvt) \
	ast_debug(5, "[%s] lock\n", pvt->id); ast_mutex_lock(&pvt->lock);

#define pvt_unlock(pvt) \
	ast_debug(5, "[%s] unlock\n", pvt->id); ast_mutex_unlock(&pvt->lock);

#else
*/

#define pvt_lock(pvt) ast_mutex_lock(&pvt->lock);
#define pvt_unlock(pvt) ast_mutex_unlock(&pvt->lock);

//#endif
